import { useEffect, useState } from "react";
import CustomerCard from "./CustomerCard";
import LinearIndeterminate from "./LinearLoading";
import AdminButton from "../buttons/AdminButton";

function Customers() {
    const [customers, setCustomers] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    const fetchCustomers = () => {
        fetch('http://localhost:9000/customers')
        .then(response => {
            return response.json()
        })
        .then(json => {
            console.log(json)
            setCustomers(json)
        })
        .catch(error => {
            setError(`Something went wrong while fetching data!`)
            console.log('An error occured while fetching data: ', error)
        })
    }

    useEffect(() => {
        fetchCustomers()
    }, [])

    useEffect(() => {
        setIsLoading(false)
    }, [customers])
    
    return (
        <div>
            {isLoading ? <LinearIndeterminate/> : 
            <div>
                <div style={{display: "flex", justifyContent: "center", marginTop: "30px"}}>
                    <AdminButton 
                        link={"/create-customer"} 
                        text={"Create new customer"}/>
                    <AdminButton 
                        link={"/create-account"} 
                        text={"Add account to customer"}/>
                    <AdminButton 
                        link={"/delete-account"} 
                        text={"Delete account from customer"}/>
                </div>

                <p style={{textAlign: "center", fontSize: 30, fontWeight: 700}}>All the customers</p>

                {error && <p style={{ color: 'red', textAlign: "center", fontWeight: 700, fontSize: "20px" }}>{error}</p>}
                <ol style={{"padding": 0}}>

                    {customers && customers.map(customer => <CustomerCard
                                                    key={customer.id}
                                                    id={customer.id}
                                                    name={customer.name}
                                                    email={customer.email}
                                                    age={customer.age}
                                                    accounts={customer.accounts}                                                
                                                />)}

                </ol>
            </div>}
        </div>
    );
}

export default Customers;