import CustomerAccountsTable from "./CustomerAccountsTable";

function CustomerCard(props) {
    let {id, name, email, age, accounts} = props;

    return (
        <div style={ {"border": "2px solid black", "margin": "10px auto", "padding": "20px", "max-width": "800px"} }>
            <p>Id: {id}. Name: {name}</p>
            <p>Email: {email}</p>
            <p>Age: {age}</p>
            
            {accounts.length === 0 ? <></> : <CustomerAccountsTable
                key={id}
                accounts={accounts}
            />}
            
        </div>
    )
}

export default CustomerCard;