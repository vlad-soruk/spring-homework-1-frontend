import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

function CustomerAccountsTable(props) {
    let {accounts} = props;

    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Account Id</TableCell>
                        <TableCell align="right">Number</TableCell>
                        <TableCell align="right">Currency</TableCell>
                        <TableCell align="right">Balance</TableCell>
                    </TableRow>
                </TableHead>
                
                <TableBody>
                    {accounts.map( account => (
                        <TableRow
                            key={account.id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {account.id}
                            </TableCell>
                            <TableCell align="right">{account.number}</TableCell>
                            <TableCell align="right">{account.currency}</TableCell>
                            <TableCell align="right">{account.balance}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );    
}

export default CustomerAccountsTable