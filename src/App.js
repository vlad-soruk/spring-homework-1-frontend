import { Route, Routes } from 'react-router-dom';
import './App.css';
import Customers from './myBusiness/Customers';
import CreateCustomerForm from './createCustomerForm/CreateCustomerForm';
import CreateAccountForm from './createAccountForm/CreateAccountForm';
import DeleteAccountForm from './deleteAccountForm/DeleteAccountForm';

function App() {
  return (
    <>
      <Routes>
        <Route path='/*' element={<Customers/>}/>
        <Route path='/create-customer' element={<CreateCustomerForm/>}/>
        <Route path='/create-account' element={<CreateAccountForm/>}/>
        <Route path='/delete-account' element={<DeleteAccountForm/>}/>
      </Routes>
    </>
  );
}

export default App;
