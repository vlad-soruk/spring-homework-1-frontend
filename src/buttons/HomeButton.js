import style from './Button.module.scss'

export default function HomeButton() {
    return (
        <>
        <a href="/" target="_self">
                <button className={style.homeButton}>Go to HOME page</button>
            </a>
        </>
    ) 
}