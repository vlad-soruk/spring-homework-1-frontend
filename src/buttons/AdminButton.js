import style from './Button.module.scss'

export default function AdminButton(props) {
    let {link, text} = props;

    return (
        <>
            <a href={link} target='_self'>
                <button className={style.createCustomerButton}>{text}</button>
            </a>
        </>
    ) 
}