import style from "./Button.module.scss"

function SubmitButton({text}) {
    return (
        <>
            <button type="submit" className={style.submitButton}>{text}</button>
        </>
    )
}

export default SubmitButton;