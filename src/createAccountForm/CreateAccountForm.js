import SubmitButton from "../buttons/SubmitButton";
import "../App.css"
import { useState } from "react";
import HomeButton from "../buttons/HomeButton";

function CreateAccountForm() {
    const [successCreation, setSuccessCreation] = useState();
    const [error, setError] = useState();

    const [customerId, setCustomerId] = useState();
    const [accountCurrency, setAccountCurrency] = useState({
        currency: "UAH"
    });

    const handleInputChange = (event) => {
        const {name, value} = event.target;

        setSuccessCreation(null);
        setError(null);

        if (name === "customerId") {
            setCustomerId(value);
        }
        else {
            setAccountCurrency(() => ({
                currency: value
            }));
        }
    }

    const createAccountFormSubmit = (event) => {
        event.preventDefault();
        console.log(accountCurrency);
        console.log(JSON.stringify(accountCurrency));

        fetch(`http://localhost:9000/accounts/create-account/${customerId}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(accountCurrency)
        })
        .then(res => {
            console.log(res);
            if (res.ok) {
                setError(null);
                setSuccessCreation(`New account was successfully added to customer with id ${customerId}!`);
            } 
            else {
                setSuccessCreation(null);
                console.log("ERROR OCURRED! RESPONSE!!!");
                console.log(res);
                setError(`HTTP error! Status: ${res.status}`);
            }
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error);
        })
    }

    return (
        <div>
            {successCreation && <p className="informationText">{successCreation}</p>}            
            {error && <p className="informationText error">{error}</p>}

            <form className="createAccountForm" onSubmit={createAccountFormSubmit}>
                <select name="accountCurrency" onChange={handleInputChange} className="currencyInput">
                    <option value="UAH">UAH</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="GBP">GBP</option>
                    <option value="CHF">CHF</option>
                </select>
                <input 
                    className="formInput"
                    type="number" 
                    name="customerId" 
                    value={customerId} 
                    onChange={handleInputChange}
                    placeholder="Enter customer ID"
                    required/>
                <SubmitButton text="Create account!"/>
            </form>
            <div style={{display: "flex", justifyContent: "center"}}>
                <HomeButton/>
            </div>
        </div>
    )
}

export default CreateAccountForm;