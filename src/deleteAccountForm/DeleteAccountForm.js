import SubmitButton from "../buttons/SubmitButton";
import "../App.css"
import { useState } from "react";
import HomeButton from "../buttons/HomeButton";


function DeleteAccountForm() {
    const [successDeletion, setSuccessDeletion] = useState();
    const [error, setError] = useState();

    const [accountData, setAccountData] = useState({
        customerId: "",
        accountNumber: ""
    });

    const handleInputChange = (event) => {
        const {name, value} = event.target;

        setSuccessDeletion(null);
        setError(null);

        setAccountData((prevData) => ({
            ...prevData,
            [name]: value
        }))
    }

    const deleteAccountFormSubmit = (event) => {
        event.preventDefault();
        console.log(accountData);

        fetch(`http://localhost:9000/accounts/delete-account?customerId=${accountData.customerId}&number=${accountData.accountNumber}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            },
        })
        .then(res => {
            console.log(res);
            if (res.ok) {
                setError(null);
                setSuccessDeletion(`Account with number ${accountData.accountNumber} was successfully deleted!`);
            } 
            else {
                setSuccessDeletion(null);
                console.log("ERROR OCURRED! RESPONSE!!!");
                console.log(res);
                setError(`HTTP error! Status: ${res.status}`);
            }
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error);
        })
    }

    return (
        <div>
            {successDeletion && <p className="informationText">{successDeletion}</p>}            
            {error && <p className={`informationText error`}>{error}</p>}

            <form className="deleteAccountForm" onSubmit={deleteAccountFormSubmit}>
                <input 
                    className="formInput"
                    type="text" 
                    name="accountNumber" 
                    value={accountData.accountNumber} 
                    onChange={handleInputChange}
                    placeholder="Enter account number you want to delete"
                    required/>
                <input 
                    className="formInput"
                    type="number" 
                    name="customerId" 
                    value={accountData.customerId} 
                    onChange={handleInputChange}
                    placeholder="Enter customer ID"
                    required/>
                <SubmitButton text="Delete account!"/>
            </form>
            <div style={{display: "flex", justifyContent: "center"}}>
                <HomeButton/>
            </div>
        </div>
    )
}

export default DeleteAccountForm;