import SubmitButton from "../buttons/SubmitButton";
import "../App.css"
import { useState } from "react";
import HomeButton from "../buttons/HomeButton";

function CreateCustomerForm() {
    const [successCreation, setSuccessCreation] = useState();
    const [error, setError] = useState();

    const [customerData, setCustomerData] = useState({
        name: "",
        email: "",
        age: ""
    });

    const handleInputChange = (event) => {
        const {name, value} = event.target;
        console.log(event.target);

        setSuccessCreation(null);
        setError(null);

        setCustomerData((prevData) => ({
            ...prevData,
            [name]: value
        }))
    }

    const createCustomerFormSubmit = (event) => {
        event.preventDefault();
        console.log(customerData);
        JSON.stringify(customerData);

        fetch("http://localhost:9000/customers/create", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(customerData)
        })
        .then(res => {
            if (res.ok) {
                setSuccessCreation(`New customer ${customerData.name} was successfully created!`);
                return res.json();
            } 
            else {
                setSuccessCreation(null);
                console.log("RESPONSE!!!");
                console.log(res);
                setError(`HTTP error! Status: ${res.status}`);
            }
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error);
        })
    }

    return (
        <div>
            {successCreation && <p className="informationText">{successCreation}</p>}
            {error && <p className="informationText error">{error}</p>}
            <form className="createCustomerForm" onSubmit={createCustomerFormSubmit}>
                <input 
                    className="formInput"
                    type="text" 
                    name="name" 
                    value={customerData.name} 
                    onChange={handleInputChange}
                    placeholder="Enter name"
                    required/>
                <input 
                    className="formInput"
                    type="email" 
                    name="email" 
                    value={customerData.email} 
                    onChange={handleInputChange}
                    placeholder="Enter email"
                    required/>
                <input 
                    className="formInput"
                    type="text" 
                    name="age" 
                    value={customerData.age} 
                    onChange={handleInputChange}
                    placeholder="Enter age"
                    required/>
                <SubmitButton text="Create customer!"/>
            </form>
            <div style={{display: "flex", justifyContent: "center"}}>
                <HomeButton/>
            </div>
        </div>
    )
}

export default CreateCustomerForm;